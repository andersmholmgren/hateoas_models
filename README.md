# REST Handler for Dart Shelf

[![Build Status](https://drone.io/bitbucket.org/andersmholmgren/hateoas_models/status.png)](https://drone.io/bitbucket.org/andersmholmgren/hateoas_models/latest)
[![Pub Version](http://img.shields.io/pub/v/hateoas_models.svg)](https://pub.dartlang.org/packages/hateoas_models)


## Introduction

Provides [Shelf](https://api.dartlang.org/apidocs/channels/be/dartdoc-viewer/shelf) models to help with HATEOAS

This is totally experimental and usage is currently discouraged ;-)

This package is being developed in conjunction with HATEOAS support in shelf_rest.
When that is complete then this package is likely to be fleshed out, documented and ready for use