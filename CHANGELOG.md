## 0.2.0

- Strong mode changes. Changes to some generics

## 0.1.7

* Update dependency versions on uri package

## 0.1.0

* Many changes

## 0.0.1+1

* removed unused dependency

## 0.0.1

* experimental HATEOAS support
