// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.hateoas;

import 'src/preconditions.dart';
import 'package:option/option.dart';
import 'package:uri/uri.dart';
import 'package:path/path.dart' as path;

final _p = path.url;

class Link {
  final String rel;
  final String href;
  Uri get hrefAsUri => Uri.parse(href);
  UriTemplate get hrefAsUriTemplate => new UriTemplate(href);

  Link(String rel, String href)
      : this.rel = checkNotNull(rel),
        this.href = checkNotNull(href);

  Link.uri(String rel, Uri href) : this(rel, href.toString());

  Link.uriTemplate(String rel, UriTemplate href) : this(rel, href.template);

  Link.self(Uri href) : this.uri('self', href);

  static Option<Link> optUri(String rel, Option<Uri> hrefOpt) =>
      hrefOpt.map((href) => new Link.uri(rel, href));

  static Option<Link> optUriTemplate(String rel, Option<UriTemplate> hrefOpt) =>
      hrefOpt.map((href) => new Link.uriTemplate(rel, href));

  Link.fromJson(Map<String, Object> json) : this(json['rel'], json['href']);

  Map<String, Object> toJson() => {'rel': rel, 'href': href};

  @override
  int get hashCode => href.hashCode;

  @override
  bool operator ==(other) =>
      other is Link && other.rel == rel && other.href == href;

  @override
  String toString() => 'Link($rel : $href)';
}

abstract class Links {
  Iterable<Link> get links;

  Uri get self => findUriByRel('self').get();

  Option<Link> findByRel(String rel) =>
      new Option(links.firstWhere((l) => l.rel == rel, orElse: () => null));

  Option<Uri> findUriByRel(String rel) =>
      findByRel(rel).map((l) => l.hrefAsUri);

  Option<UriTemplate> findUriTemplateByRel(String rel) =>
      findByRel(rel).map((l) => l.hrefAsUriTemplate);

  Map toJson() =>
      {'links': links.map((l) => l.toJson()).toList(growable: false)};

  void addJson(Map parentJson) => parentJson.addAll(toJson());
}

class DefaultLinks extends Links {
  final Iterable<Link> links;

  DefaultLinks(this.links);

  DefaultLinks.fromJson(Map json) : this(new LinkJsonHelper(json).links);
}

class SearchLinks extends DefaultLinks {
  Option<Uri> get previousPage => findUriByRel('previousPage');
  Option<Uri> get nextPage => findUriByRel('nextPage');

  SearchLinks(Iterable<Link> links) : super(links);

  SearchLinks.fromJson(Map json) : super.fromJson(json);
}

abstract class Jsonable {
  Map toJson();
}

abstract class ModelWithLinks<L extends Links, M extends Jsonable>
    implements Jsonable {
  final L links;
  final M model;

  ModelWithLinks(this.model, this.links);

  Map toJson() => model.toJson()..addAll(links.toJson());
}

abstract class ResourceModel<R extends Jsonable>
    extends ModelWithLinks<ResourceLinks, R> {
  ResourceModel(R model, ResourceLinks links) : super(model, links);
}

abstract class Identified {
  String get id;
}

class SearchResult<R> implements Jsonable {
  final int totalCount;
  final int page;
  final List<R> results;
  final int numPages;
  int get pageSize => numPages > 0 ? (totalCount / numPages).ceil() : 1;

  SearchResult(int totalCount, int numPages, int page, this.results)
      : this.totalCount = checkNotNull(totalCount),
        this.numPages = checkNotNull(numPages),
        this.page = checkNotNull(page) {
    ensure(totalCount, greaterThanOrEqualTo(0));
    ensure(numPages, greaterThanOrEqualTo(0));
    ensure(page, greaterThanOrEqualTo(0));
  }

  SearchResult.fromJson(Map json, R resourceFromJson(Map resourceJson))
      : this(
            json['totalCount'],
            json['numPages'],
            json['page'],
            (json['results'] as Iterable)
                .map(resourceFromJson)
                .toList(growable: false));

  Map toJson() => {
        'totalCount': totalCount,
        'numPages': numPages,
        'page': page,
        'results': results
      };
}

class SearchResourceModel<R extends Jsonable, RM extends ResourceModel<R>>
    extends ModelWithLinks<SearchLinks, SearchResult<RM>> {
  SearchResult<RM> get results => model; // TODO:???

  SearchResourceModel(SearchResult<RM> result, SearchLinks links)
      : super(result, links);

  SearchResourceModel.fromJson(Map json, RM resourceFromJson(Map resourceJson))
      : this(new SearchResult<RM>.fromJson(json, resourceFromJson),
            new SearchLinks.fromJson(json));
}

abstract class LinksBuilder<L extends Links> {
  final List<Link> _links;
  final String _childPrefix;

  LinksBuilder._(this._links, this._childPrefix);
  LinksBuilder() : this._([], '');

  void child(String childName, childBuilder(LinksBuilder c)) {
    final childLinksBuilder = createChild(_links,
        _childPrefix.isNotEmpty ? '${_childPrefix}$childName' : '$childName.');
    childBuilder(childLinksBuilder);
  }

  void add(String rel, Object href) {
    if (href != null) {
      final _rel = _childPrefix.isNotEmpty ? '$_childPrefix$rel' : rel;
      _links.add(new Link(_rel, href.toString()));
    }
  }

  void addUriTemplate(String rel, UriTemplate uri) {
    add(rel, uri);
  }

  void addSelf(Uri uri) {
    add('self', uri);
  }

  L build();

  LinksBuilder<L> createChild(List<Link> links, String childPrefix);
}

class DefaultLinksBuilder extends LinksBuilder<DefaultLinks> {
  DefaultLinksBuilder._(List<Link> links, String childPrefix)
      : super._(links, childPrefix);
  DefaultLinksBuilder() : super();

  DefaultLinks build() => new DefaultLinks(_links);

  DefaultLinksBuilder createChild(List<Link> links, String childPrefix) =>
      new DefaultLinksBuilder._(links, childPrefix);
}

class SearchLinksBuilder extends LinksBuilder<SearchLinks> {
  SearchLinksBuilder._(List<Link> links, String childPrefix)
      : super._(links, childPrefix);
  SearchLinksBuilder() : super();

  SearchLinks build() => new SearchLinks(_links);

  SearchLinksBuilder createChild(List<Link> links, String childPrefix) =>
      throw new ArgumentError('not implemented'); // TODO: dodgy

  void addPreviousPage(Uri previousPage) => add('previousPage', previousPage);
  void addNextPage(Uri nextPage) => add('nextPage', nextPage);
}

typedef ResourceLinks ResourceLinksFactoryFunction(resourceId,
    {bool includeSelf});

typedef IdExtractor(model);

abstract class ResourceLinksFactory {
  ResourceLinks call(resourceId) => forResourceId(resourceId);

  ResourceLinks forResourceId(resourceId, {bool includeSelf: true});

  SearchResult /*<R>*/ forCollection /*<R>*/ (int totalCount, int numPages,
      int page, Iterable results, /*=R*/ convert(model, links),
      [IdExtractor getId]) {
    final mappedResults = results.map /*<R>*/ ((model) {
      final _getId = getId != null ? getId : (v) => v.id;
      return convert(model, forResourceId(_getId(model), includeSelf: false));
    });
    return new SearchResult /*<R>*/ (
        totalCount, numPages, page, mappedResults.toList(growable: false));
  }

  // TODO: would be nice to support generating prev / next links but too much
  // work for now
//  SearchResourceModel forCollection(int totalCount, int numPages, int page,
//                                     Iterable results, convert(model, links),
//                             [IdExtractor getId]) {
//    // hmmm how do we create the previous next links???
//    // we do have access to the route and if we know the name of the page
//    // param name (defaulting to page) we could substitute.
//    // would be nice to have uriTemplate to partial substitutions
//  }

}

class DefaultResourceLinksFactory extends ResourceLinksFactory {
  final ResourceLinksFactoryFunction function;

  DefaultResourceLinksFactory(this.function);

  @override
  ResourceLinks forResourceId(resourceId, {bool includeSelf: true}) =>
      function(resourceId, includeSelf: includeSelf);
}

class ResourceLinks extends DefaultLinks {
  Option<Uri> get delete => findUriByRel('delete');
  Option<Uri> get update => findUriByRel('update');

  ResourceLinks(Iterable<Link> links) : super(links);

  ResourceLinks.fromJson(Map json) : super.fromJson(json);

  ResourceCollectionLinks childLinks(String name) =>
      new ResourceCollectionLinks._(this, name);

  Option<UriTemplate> childLink(String childName, String rel) =>
      childLinks(childName).findByRel(rel).map((l) => l.hrefAsUriTemplate);
}

class ResourceCollectionLinks {
  final ResourceLinks _parent;
  final String qualifier;

  ResourceCollectionLinks._(this._parent, this.qualifier);

  String _qualifiedRel(String rel) => '$qualifier.$rel';

  Option<Link> findByRel(String rel) => _parent.findByRel(_qualifiedRel(rel));

  Option<UriTemplate> findHRefByRel(String rel) =>
      _parent.findUriTemplateByRel(_qualifiedRel(rel));

  // template: <parentId>/childPath/<search params>
  Option<UriTemplate> get search => findHRefByRel('search');

  // template: <parentId>/childPath/{child path param}
  Option<UriTemplate> get find => findHRefByRel('find');

  // template: <parentId>/childPath/{child path param}
  // or <parentId>/childPath for POST
  Option<UriTemplate> get create => findHRefByRel('create');

  // template: <parentId>/childPath/{child path param}
  Option<UriTemplate> get update => findHRefByRel('update');

// template: <parentId>/childPath/{child path param}
  Option<UriTemplate> get delete => findHRefByRel('delete');
}

class ResourceLinksBuilder {
  final List<Link> _links;
  final String _childPrefix;

  ResourceLinksBuilder._(this._links, this._childPrefix);
  ResourceLinksBuilder() : this._([], '');

  void child(String childName, childBuilder(ResourceLinksBuilder c)) {
    final childLinksBuilder = new ResourceLinksBuilder._(_links,
        _childPrefix.isNotEmpty ? '${_childPrefix}$childName' : '$childName.');
    childBuilder(childLinksBuilder);
  }

  void add(String rel, Object href) {
    if (href != null) {
      final _rel = _childPrefix.isNotEmpty ? '$_childPrefix$rel' : rel;
      _links.add(new Link(_rel, href.toString()));
    }
  }

  void addUriTemplate(String rel, UriTemplate uri) {
    add(rel, uri);
  }

  void addSelf(Uri uri) {
    add('self', uri);
  }

  void addDelete(Object uri) {
    add('delete', uri);
  }

  void addUpdate(Object uri) {
    add('update', uri);
  }

  void addSearch(Object uri) {
    add('search', uri);
  }

  void addFind(Object uri) {
    add('find', uri);
  }

  void addCreate(Object uri) {
    add('create', uri);
  }

  ResourceLinks build() => new ResourceLinks(_links);
}

class LinkJsonHelper {
  List<Link> links;

  LinkJsonHelper(Map json) {
    final linkJson = (json['links'] as List<Map<String, Object>>);

    links =
        linkJson.map /*<Link>*/ ((link) => new Link.fromJson(link)).toList();
  }

  _link(String rel, convert(Link link)) {
    final result = links.where((l) => l.rel == rel);
    return result.isEmpty ? null : convert(result.first);
  }

  Uri uri(String rel) => _link(rel, (link) => link.hrefAsUri);

  UriTemplate uriTemplate(String rel) =>
      _link(rel, (link) => link.hrefAsUriTemplate);
}

class LinkHelper {
  final List<Link> links;

  LinkHelper([self]) : links = self != null ? [new Link.self(self)] : [];

  void addLink(String rel, href) {
    if (href != null) {
      links.add(new Link(rel, href.toString()));
    }
  }
}
